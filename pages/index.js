import Head from 'next/head'
import React, { useState } from "react";
import Layout from '../components/layout'
import utilStyles from '../styles/utils.module.css'

export default function Home() {
  const [showAlt, setShowAlt] = useState(false);

  function toggle(){
    setShowAlt(!showAlt);
    document.documentElement.classList.toggle("showAlt");
  }

  const elements = [
    {
      name: 'star',
      text: 'My Paintings',
      newTab: true,
      href: 'https://www.instagram.com/cra_sto/',
      classes : [utilStyles.element, utilStyles.shadow, utilStyles.shadowHover, utilStyles.star],
    },
    {
      name: 'eyes',
      text: 'My Music',
      newTab: true,
      href: 'http://hyperurl.co/oor0nv',
      classes : [utilStyles.element, utilStyles.shadow, utilStyles.shadowHover, utilStyles.flipHover, utilStyles.eyes],
    },
    {
      name: 'falce',
      text: 'Contact',
      href: '/contact',
      classes : [utilStyles.element, utilStyles.shadow, utilStyles.shadowHover, utilStyles.falce],
    },
    {
      name: 'pestello',
      text: 'Archive',
      href: '/archive',
      classes : [utilStyles.element, utilStyles.shadow, utilStyles.shadowHover, utilStyles.pestello],
    },
    {
      name: 'stone',
      text: '',
      href: null,
      hidden: showAlt,
      onClick: toggle,
      classes : [utilStyles.element, utilStyles.shadow, utilStyles.shadowHover, utilStyles.stone],
    },
    {
      name: 'world',
      text: '',
      href: null,
      hidden: !showAlt,
      onClick: toggle,
      classes : [utilStyles.element, utilStyles.shadow, utilStyles.shadowHover, utilStyles.world],
    },
  ]

  return (
    <Layout home>
      <Head>
        <title>Crasto</title>
        <link rel="preload" as="image" href="/images/world.png"/>
        <link rel="preload" as="image" href="/images/checkers.png"/>
        <link rel="preload" as="image" href="/images/runestone.png"/>
      </Head>
        {elements.map((element, index) => !element.hidden && (
          <a className={element.classes.join(' ')}
            key={index}
            onClick={element.onClick}
            role={element.href ? null : "button"}
            aria-label={element.text ? null : element.name}
            href={element.href}
            rel={element.href && element.newTab && "noopener"}
            target={element.href && element.newTab && "_blank"}>
              {element.text && <div className={utilStyles.elementText}>{element.text}</div> }
              <img src={`/images/${element.name}.png`} width="100" height="100" alt={element.text}/>
          </a>
        ))}
    </Layout>
  )
}