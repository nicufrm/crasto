import Head from 'next/head'
import Layout from '../components/layout'
import utilStyles from '../styles/utils.module.css'

export default function Contact() {
  return (
    <Layout home>
      <Head>
        <title>Crasto - Thanks</title>
      </Head>
        <div className={utilStyles.thanks}>
          <div>Message Sent!</div>
          <div>
            <a href='/'>click here to go back to the homepage</a>
          </div>
        </div>
    </Layout>
  )
}