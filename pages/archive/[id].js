import React, { useState } from "react";
import { useRouter } from 'next/router';
import Head from 'next/head'
import Layout from '../../components/layout'
import utilStyles from '../../styles/utils.module.css'
import { getAllArchiveIds } from '../../lib/archive'

export default function Archive({ data }) {
  const router = useRouter()
  const list = data[0]
  const fileName = data[1] || null;
  const [Image, setImage] = useState(fileName);

  const cleanName = name => name && name.replace(/\.jpg$/, '').replace(/\.jpeg$/, '').replace(/\.png$/, '').replace(/\.gif$/, '')


  function navigateToImage(e,img) {
    e.preventDefault()
    setImage(img);
    router.push(`/archive/${img}`, null, {scroll: true, shallow: true})
    window.scrollTo(0, 0);
  }

  return (
    <Layout home>
      <Head>
        <title>Crasto - Archive</title>
        <style dangerouslySetInnerHTML={{__html: `
          @media only screen and (orientation: portrait), only screen and (max-width: 768px) {
            html { background-size: 300vh !important; }
          }
        `}} />
      </Head>
      <div className={utilStyles.archive}>
        <div className={[utilStyles.archiveMenu, utilStyles.archiveColumn].join(' ')}>
          {list.map(img =>
            <a key={img} onClick={(e) => navigateToImage(e,img)} href={`/archive/${img}`}>{cleanName(img)}</a>
          )}
        </div>
        {Image &&
          <div className={[utilStyles.archiveContent, utilStyles.archiveColumn].join(' ')}>
            <img src={`/archive-images/${Image}`} className={utilStyles.archiveImage} alt={cleanName(Image)}/>
            <div className={utilStyles.archiveTitle}>{cleanName(Image)}</div>
          </div>
        }
      </div>
    </Layout>
  )
}

export async function getStaticPaths() {
  const paths = getAllArchiveIds().map(fileName => {
    return {
      params: {
        id: fileName
      }
    }
  })

  return {
    paths,
    fallback: false
  }
}

export async function getStaticProps({ params }) {
  const paths = getAllArchiveIds()
  return {
    props: {
      data: [
        paths,
        params.id
      ]
    }
  }
}