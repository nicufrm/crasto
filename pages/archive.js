import Archive from './archive/[id]'
import { getAllArchiveIds } from '../lib/archive'

export default Archive

export async function getStaticProps() {
  const paths = getAllArchiveIds()
  return {
    props: {
      data: [
        paths
      ]
    }
  }
}