import Head from 'next/head'
import Layout from '../components/layout'
import utilStyles from '../styles/utils.module.css'

export default function Contact() {
  return (
    <Layout home>
      <Head>
        <title>Crasto - Contact</title>
        <style dangerouslySetInnerHTML={{__html: `
          html {
            background-color: #FFFFFF !important;
            background-image: url('/images/checkers.png') !important;
            background-size: 1vw !important;
          }
        `}} />
      </Head>
        <img
            src="/images/runestone.png"
            className={[utilStyles.shadow, utilStyles.runestone].join(' ')}
            alt='Crasto'
        />
        <form action="https://formsubmit.co/b0dc45b5ffc12cc27ae21e30b7f39149" className={utilStyles.form} method="POST">
            <div className={utilStyles.formLine}>
              <label for="name">Name:</label>
              <input type="text" name="name" id="name" required />
            </div>
            <div className={utilStyles.formLine}>
              <label for="email">Email:</label>
              <input type="email" name="email" id="email" required />
            </div>
            <div className={utilStyles.formLine}>
              <label for="email">Message:</label>
              <textarea name="message" rows="7"></textarea>
            </div>
            <input type="hidden" name="_subject" value="Message from CRASTO website"></input>
            <input type="hidden" name="_next" value="https://crasto.vercel.app/thanks"></input>
            <input type="text" name="_honey" style={{display: 'none'}}></input>
            {/* <input type="hidden" name="_captcha" value="false"></input> */}
            <div className={utilStyles.formLine}>
              <button type="submit">Send Message</button>
            </div>
        </form>
    </Layout>
  )
}