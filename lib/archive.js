import fs from 'fs'
import path from 'path'

const archiveDirectory = path.join(process.cwd(), 'public/archive-images')

export function getSortedArchiveData() {
  const fileNames = fs.readdirSync(archiveDirectory)
  const allArchiveData = fileNames.map(fileName => {
    const id = fileName
    return { id }
  })
  return allArchiveData.sort((a, b) => {
    if (a.id < b.id) {
      return 1
    } else {
      return -1
    }
  })
}

export function getAllArchiveIds() {
  const fileNames = fs.readdirSync(archiveDirectory)
  const list = fileNames.map(fileName => fileName);
  return list;
}