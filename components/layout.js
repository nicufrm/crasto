import Head from 'next/head'
import utilStyles from '../styles/utils.module.css'

export default function Layout({ children, home }) {
  return (
    <div className={utilStyles.container}>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <meta name="description" content="Crasto" />
        <title>Crasto</title>
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
        <link rel="manifest" href="/site.webmanifest" />
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5" />
        <meta name="msapplication-TileColor" content="#da532c" />
        <meta name="theme-color" content="#ffffff"></meta>
      </Head>
      <a href="/" className={[utilStyles.shadow, utilStyles.shadowHover, utilStyles.logo].join(' ')}>
        <img src="/images/crasto.png" width="504" height="128" alt='Crasto'/>
      </a>

      {children}
    </div>
  )
}
